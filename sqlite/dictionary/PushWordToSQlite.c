#include<stdio.h>
#include<stdlib.h>
#include<sqlite3.h>
#include<string.h>

/*
 *This code tries to push all data from dict.txt to sqlite3
 *
 *
 */

#define WORD_MAX    64 
#define SQ_CMD_MAX  512 * 2
#define STRING_MAX  512 

sqlite3* init_sqlite(void);

char * init_word(char *data);

int push_word_into_sqlite(sqlite3*,char*);


int main(int argc, const char *argv[])
{

    sqlite3 *data = NULL;

    data = init_sqlite();                      //init sqlite and  create a table if necessary

    FILE *fp = NULL;
    if((fp = fopen("dict.txt","r")) == NULL){
        perror("fopen is error!");
        exit(0);
    }

    char temp[STRING_MAX] = {0};
    char *p_word;

    while(fgets(temp,sizeof(temp),fp) != NULL){

        p_word = init_word(temp);               //init the word that you want search

        printf("p_word****:%s\n",p_word);

        push_word_into_sqlite(data,p_word);

    }

    puts("insert the dictionary into sqlite has done!");

    return 0;
}


sqlite3* init_sqlite(void){
    char    sqlcmd[SQ_CMD_MAX] = {0};
    int     result = 0;
    char*   errmsg;
    sqlite3* data;
    
    if((result = sqlite3_open("my.db",&data)) != SQLITE_OK){
        printf("sqlite3_open result:%d\n",result);
        return NULL;
    }

    sprintf(sqlcmd,"create table dict(word text primary key,meaning text not null)");

    if((result = sqlite3_exec(data,sqlcmd,NULL,NULL,&errmsg)) != SQLITE_OK){
        puts(errmsg);
    }

    return data;

}

/*
 *  Function : init_word 
 *  it searches for ' and " from the string 
 *  if it has ' or " ,add \ befor it
 *  return: a new string which has ' \'' or '\"'
 */

char * init_word(char *data){
    
    char * new = NULL;
    char * temp;

    if((new = (char*)malloc(sizeof(char) * STRING_MAX)) == NULL){
        puts("malloc error!");
        return NULL;
    }
    temp = new;

    while(*data){
        if(*data == '\'' ){
            *temp++ = '\''; // before "'" insert another "'" can insert a "'" into sqlite3 
        }
        *temp++ = *data++; 
    }
    *temp = *data; //copy the '\0'

    return new;
}


int push_word_into_sqlite(sqlite3 * data,char *p_word){

    char sqlcmd     [SQ_CMD_MAX] = {0};
    char word       [WORD_MAX]   = {0};
    char meaning    [STRING_MAX] = {0};

    char * errmsg       = NULL;
    char * temp         = word;
    char * temp_p_word  = p_word;

    while(*p_word != ' '){
        *temp++ = *p_word++;
    }

    while(*p_word == ' ')
        p_word++;

    temp  = meaning;

    while((*temp++ = *p_word++) != '\0')
        ;

    printf("%s word:%s meaning:%s\n",__FUNCTION__,word,meaning);

    sprintf(sqlcmd,"insert into dict values('%s','%s')",word,meaning);

    puts(sqlcmd);

    if(sqlite3_exec(data,sqlcmd,NULL,NULL,&errmsg) != SQLITE_OK){
        printf("insert into is error!:%s\n",errmsg);
    }else{
        printf("insert into success!\n");
    }


    printf("return from push_word_into_sqlite \n");

    free(temp_p_word);
    temp_p_word = NULL;
    
    return 0;
}
