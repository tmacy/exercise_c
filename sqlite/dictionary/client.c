#include "dict.h"


void do_register(int);
void do_login(int);
void do_query(int,char *name);
void do_history(int,char *name);
int dictionary(int,char *);

int main(int argc, const char *argv[])
{
    int socketfd;
    struct sockaddr_in server_addr;
    
    if(argc < 3){
        printf("usage : %s <server_ip> <server_port>\n",argv[0]);
        exit(-1);
    }

    if((socketfd = socket(PF_INET,SOCK_STREAM,0)) < 0){
        perror("fail to socket");
        exit(-1);
    }

    bzero(&server_addr,sizeof(server_addr));
    server_addr.sin_family = PF_INET;
    server_addr.sin_addr.s_addr = inet_addr(argv[1]);
    server_addr.sin_port = htons(atoi(argv[2]));

    if(connect(socketfd,(struct sockaddr*)&server_addr,sizeof(server_addr)) < 0){
        perror("fail to connect");
        exit(-1);
    }

    int input;

    while(1){

        putchar(10);
        putchar(10);
        puts("*******************************************");
        puts("*      Welcom to online dictionary        *");
        puts("*      1.register 2.login  3.quit         *");
        puts("*******************************************");
        putchar(10);
        putchar(10);

        
        puts("input a number in (1-3):");

        if(scanf("%d",&input) <= 0){
            perror("in scanf");
            setbuf(stdin,NULL);//clear input buffer
            continue; 
        }

        switch(input){
        case 1:
            do_register(socketfd); 
            break;
        case 2:
            puts("2");
            do_login(socketfd);
            break;
        case 3:
            puts("3");
            exit(0);
        }
    }

    close(socketfd);
    return 0;
}


void do_register(int socketfd){

    message msg;

    bzero(&msg,sizeof(message));
    msg.type = R;
    puts("input :your user name");
    scanf("%s",msg.name);
    puts("input your user passwd");
    scanf("%s",msg.data);
    
    send(socketfd,&msg,sizeof(message),0);
    recv(socketfd,&msg,sizeof(message),0);

    printf("register :%s\n",msg.data);
    return;
}
void do_login(int socketfd){

    message msg;

    bzero(&msg,sizeof(message));
    msg.type = L;
    puts("input your usr name:");
    scanf("%s",msg.name);
    puts("input your passwd:");
    scanf("%s",msg.data);

    printf("login :%s :%s \n",msg.name,msg.data);
    send(socketfd,&msg,sizeof(msg),0);

    recv(socketfd,&msg,sizeof(msg),0);
    printf("login :%s\n",msg.data);

    if(msg.type == OK){
        dictionary(socketfd,msg.name);
    }
    return ;
}

int dictionary(int socketfd,char* name){

    while(1){
        putchar(10);
        putchar(10);
        puts("****************************");
        printf("         HELLO %s\n",name);
        puts("*    Welcom to login!      *");
        puts("*  please take a choose!   *");
        puts("*1.query  2,history  3.back*");
        puts("****************************");
        putchar(10);
        putchar(10);


        int input ;
        puts("input a number in (1-3):");

        if(scanf("%d",&input) <= 0){
            perror("error in scanf");
            setbuf(stdin,NULL);             //clear input buffer
            continue; 
        }

        switch(input){
        case 1:
            do_query(socketfd,name);
            break;
        case 2:
            do_history(socketfd,name);
            break;
        case 3:
            return 0;
        }

    }
    return 0;
}


void do_query(int socketfd,char *name){
    message msg;
    char word[DATA_MAX] = {0}; // the word you want to query
   // char meaning[DATA_MAX] = {0}; // meaning of the word from server

    msg.type = Q;

    puts("please input a word:");
    scanf("%s",word);
    
    strcpy(msg.name,name);
    strcpy(msg.data,word);

    printf("msg.name :%s msg.word :%s\n",msg.name,msg.data);

    send(socketfd,&msg,sizeof(msg),0);
    
    recv(socketfd,&msg,sizeof(msg),0);

    printf("%s : %s\n",name,msg.data);

    printf("Thanks for query ,bye!\n");
    return ;
}

void do_history(int socketfd,char* name){
    message msg = {0};
    msg.type = H;

    strcpy(msg.name,name);

    send(socketfd,&msg,sizeof(msg),0);

    puts("the word thar you have searched:");
    while(1){
        recv(socketfd,&msg,sizeof(message),0);
        if(msg.data[0] == '\0')
            break;
        printf("%s\n",msg.data);
    }
    printf("Thanks!\n");
    return ;
}


