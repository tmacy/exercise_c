#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sqlite3.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<signal.h>
#include<time.h>
#include<strings.h>
#include<string.h>


#define NAME_MAX 16
#define WORD_MAX 64
#define DATA_MAX 512 
#define OK   0
#define R    1 //user Register
#define L    2 //user Login
#define Q    3 //user query
#define H    4 //history record

typedef struct {
    int     type;
    char    name[NAME_MAX];
    char    data[DATA_MAX];
}message;


