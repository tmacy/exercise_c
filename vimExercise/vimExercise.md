
##这是一个vim练习文件可以通过这个文件练习vim的各种常用操作

学习使用Vim并且其会成为你最后一个使用的文本编辑器。没有比这个更好用的文本编辑器了，非常难学，但是却不可思议的好用。

###1.启动vim，vim在Normal模式下
###2.想要输入文本，请按下i键（注意左下角会有提示）
请在这里按下i，输入hello
###3.输入完成后，请按下ESC建，退出insert模式，返回到Normal模式。

####下面是常用的进入输入模式的方法

 * i: 从光标前进入Insert模式
 * I: 从当前行首进入Insert模式
 * a: 从光标后进入Insert模式
 * A: 从当前行尾进入Insert模式
 * o: 从当前行的下一行进入Insert模式
 * O: 从当前行的上一行进入Insert模式
 * s: 删除当前光标所在的一个字符，并进入Insert模式
 * S: 删除当前光标所在的行，并进入插入模式

####在Normal模式下，移动光标一般不推荐使用光标键，vim有自己特有的移动光标按键

   * h : 表示向左移动光标
   * l : 表示向右移动光标
   * j : 表示向下移动光标
   * h : 表示向上移动光标

#### 快速移动光标

   * 0   :数字0，表示移动到行首
   * ^   :表示移动到第一个不是blank字符的位置
   * $   :表示移动到本行行尾
   * g\_ :移动到本行最后一个不是blank字符的位置
   * NG  :移动到第N行
   * gg  :到第一行
   * G   :到最后一行
   * w   :移动到下一个单词的开头
   * e   :移动到下一个单词结尾
   * %   :匹配括号移动
   * \*和# :匹配光标所在的单词,\*是上一个，#是下一个
   * fa  :到下一个为a的字符处
   * t,  :到逗号前的第一个字符。逗号可以变为其他字符
   * 3fa : 在当前行查找第三个出现的a
   * F和T： 方向和f,t相反

#### 清除字符

   * x : 表示清除当前光标所在位置的一个字符(不进入Insert模式)
   * X : 表示清除当前光标所在位置之前的一个字符(不进入Insert模式)
   * ch : 表示清除当前光标所在位置的一个字符，并进入插入模式
   * cl : 表示清除当前光标所在位置之后的一个字符，并进入插入模式
   * cj : 表示清除当前行以及下一行，并进入插入模式
   * ck : 表示清除当前行以及上一行，并进入插入模式
   * cw : 表示清除当前光标所在的位置直到一个字的结束
   * dt“ ： 删除所有内容直到”

####搜索字符

   * /pattern :搜索pattern的字符串

####copy and paste

   * :10,20y : 复制10到20行
   * :10,20d : 剪切10到20行
   * yy      : 复制当前行
   * dd      : 剪切当前行
   * p       : 在当前光标之后粘贴
   * P       : 在当前光标之前粘贴

#### Undo / Redo / Ndo

   * u : 撤销
   * &lt;C-r> : 重做
   * . : 小数点可以让你重复上一次执行命令
   * N<command> : 重复执行某个命令N次


#### 输入命令行命令
   * ctrl + z 可以将vim 挂起
   * shell 中输入fg 找回vim

#### 打开/保存/退出
   * :e <path>                 :打开一个文件
   * :w                        : 存盘
   * ：saveas <path>           :另存为
   *  :x ZZ 或:wq              :保存并退出
   *  :q!                      :强制退出
   *  w !sudo tee %             :遇到只读文件保存的时候

####  大小写
   * gU :变大写
   * gu：变小写
   * gUU : 整行全部大写
   * guu : 整行全部小写

####区域选择 < action> a < object > 或< action >i< object >
* action 可以是任何命令，如d(删除),y(拷贝),v(可视)
* object 可以是：w 一个单词，W 以空格位分隔的单词，s 一个句子，p 一段文字，特殊字符：" ' ) } ]
* 例如("foo")
    * vi"  会选择 foo
    * va" 会选择 "foo"

#### 块操作< ctrl + v > 
* 从行首输入
    1. ^  到行头
    2. < ctrl + v > 
    3. < ctrl + d > 光标向下移动（也可以hjkl，%，其他）
    4. I --- ESC    I 可以插入，输入你想要输入的字符，ESC按下后，可以使每一行都生效
* 从行尾输入
    1. < ctrl + v >
    2. 选中相关行（j , < ctrl + d > , /pattern , % ,....)
    3. $
    4. A, 输入字符串, ESC
* < 或 >  + enter 左右缩进  
* = 自动缩进 
* :split , :vsplit (:vsp , :sp) 水平分屏，垂直分屏

