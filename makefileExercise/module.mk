CURRENTDIR =$(shell pwd)

local_dir := $(CURRENTDIR)/ecl/source
local_src := $(addprefix $(local_dir)/,calibration.c DetectBadLines.c eimCalibration.c\
		gainOffset.c gradientDetection.c heCalibration.c tc.c)

sources += $(local_src)
include_dirs += -I $(CURRENTDIR)/ecl/include/



