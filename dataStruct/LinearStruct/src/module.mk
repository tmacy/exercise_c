CURRENTDIR =$(shell pwd)
local_dir := $(CURRENTDIR)/src
local_src := $(addprefix $(local_dir)/,linklist.c linkstack.c reverse_polish.c)

sources += $(local_src)
include_dirs += -I $(local_dir)/include/

