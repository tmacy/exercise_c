/*************************************************************************
	> File Name: main.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月23日 星期三 11时30分15秒
 ************************************************************************/

#include "linklist.h"
#include "linkstack.h"

int main(void){

    char exp[64];

    printf("input a expression:\n");
    scanf("%s",exp);
    printf("%s = ",exp);

    reverse_polish(exp);
    printf("%d\n",compute(exp));
    return 0;
}
