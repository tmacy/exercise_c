/*************************************************************************
	> File Name: linkstack.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月24日 星期四 09时05分50秒
 ************************************************************************/


#include "linkstack.h"


linkstack* create_null_linkstack(){
    linkstack* s = (linkstack*)malloc(sizeof(linkstack));
    s->stack_top = create_null_linklist();
    return s;
}

int is_linkstack_empty(linkstack* s){
    assert(s != NULL && s->stack_top != NULL);
    return s->stack_top->next == NULL;
}

int push_linkstack(linkstack* s ,datatype x){
    assert(s != NULL && s->stack_top != NULL);
    return insert_linklist(s->stack_top,x,1);
}

datatype pop_linkstack(linkstack* s){
    assert(s != NULL && s->stack_top != NULL);
    return delete_linklist(s->stack_top,1);
}

datatype get_linkstack_top(linkstack* s){
    assert(s != NULL && s->stack_top != NULL);
    return  search_linklist(s->stack_top,1);
}

int clear_linkstack(linkstack *s){
    assert(s != NULL && s->stack_top != NULL);

    linknode * tmp;
    while(s->stack_top->next != NULL){
        tmp = s->stack_top->next;
        s->stack_top->next = tmp->next;
        free(tmp);
        tmp = NULL;
    }

    free(s);
    return 1;
}





