/*************************************************************************
	> File Name: linklist.h
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月23日 星期三 10时13分13秒
 ************************************************************************/
#ifndef LINKLIST
#define LINKLIST


#ifndef STDIO
#define STDIO

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>

#endif
typedef int datatype;

typedef struct node{
    datatype data;
    struct node *next;
} linknode,*linklist;

linklist create_null_linklist();

int is_linklist_empty(linklist);

int len_linklist(linklist);

int insert_linklist(linklist,datatype x,int pos);

datatype delete_linklist(linklist,int pos);

datatype search_linklist(linklist,int pos);



#endif

