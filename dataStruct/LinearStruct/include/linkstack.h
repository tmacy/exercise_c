/*************************************************************************
	> File Name: linkstack.h
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月24日 星期四 09时06分37秒
 ************************************************************************/


#ifndef LINKSTACK
#define LINKSTACK

#include"linklist.h"

typedef struct {
    linklist stack_top;
} linkstack;

linkstack* create_null_linkstack();

int is_linkstack_empty(linkstack*);

int push_linkstack(linkstack*,datatype);

datatype pop_linkstack(linkstack*);

datatype get_linkstack_top(linkstack*);

int clear_linkstack(linkstack *s);

char* reverse_polish(char* exp);

int prior(char );

int compute(char * ch);



#endif
