#include<stdio.h>

//插入排序算法
//a 是需要排序的数组，size_a是数组的大小
void insertion_sort(int *a,int size_a){
    
    int i,j;
    int key = 0;

    for(j = 1;j < size_a;j++){

        key = a[j];
        i = j - 1;
        while(i >= 0 && a[i] > key){
            a[i + 1] = a[i];
            i = i - 1;
        }
        a[i + 1] = key;
    }

    return ;
}

int main(int argc, const char *argv[])
{
    int a[10] = {10,3,6,2,7,2,6,5,1,8}; 

    insertion_sort(a,sizeof(a) / sizeof(int));

    int i;

    for(i = 0;i < sizeof(a) / sizeof(int) ;i++){
        printf("a[%d]:%d \n",i,a[i]);
    }

    return 0;
}

