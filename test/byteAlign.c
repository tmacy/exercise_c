#include<stdio.h>
/* 
 * 这个结构体大小为24,采用的4字节对齐
 * long num : 4
 * char* name :4
 * short int :2
 * char na : 1 + 1 补齐1字节
 * shortba[5] : 2 * 5 + 2 补齐2个字节
 * 总和为: 4 + 4 + 2 + 1 * 2 + 10 + 2 = 24
 *
 */
struct A{
    long num;
    char *name;
    short int data;
    char na;
    short ba[5];
};

int main(int argc, const char *argv[]){
    struct A a;
    struct A *p = &a;
    printf("&a :%p,sizeof :%d\n",p,sizeof(a));
    printf("p + 0x2:%p\n",p + 2);
    printf("(unsigned long)p + 0x2:%p\n",(unsigned long *)p + 2);
    printf("(char*)p + 0x2:%p\n",(char*)p + 2);


}
