#include<stdio.h>
#include<string.h>


/*
 *有1,2,....一直到n的无序数组,求排序算法,
 *并且要求时间复杂度为O(n),空间复杂度O(1),
 *使用交换,而且一次只能交换两个数
 */
void test1();

int main(int argc, const char *argv[])
{
    test1();

    return 0;
}


void test1(){
    int a[] = {10,3,5,6,2,8,9,1,7,4};
    int len = sizeof a / sizeof(int);
    int temp;
    for(int i = 0;i < len;){
        temp = a[a[i] - 1];
        a[a[i] - 1] = a[i];
        a[i] = temp;
        if(a[i] == i + 1)
            i++;
    }
    for(int i = 0;i < len;i++){
        printf("%d ",a[i]);
    }
    putchar(10);
    return;

}
