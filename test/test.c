#include<stdio.h>
#include<string.h>
#include<assert.h>



//#define TEST2 
#define MY_MEMCPY 



/*
 * 结构体A有三个元素
 * a占两位
 * b占两位
 * c占一位
 *
 * 单独使用a，b，c的时候
 * ，进行高位扩展.
 *
 */

struct A{
    int a:2;
    int b:2;
    int c:1;
}test = {1,3,1};


void foo(int m,int n){
    printf("m:%d,n:%d\n",m,n);
}



/**
 *求1000！的末尾有多少个0（素数相乘法：2 * 2 * 5 * 5 = 100 
 * 2 * 5 * 5 * 5 = 250)
 * 一对2,5能够成一个10因子，结果就多一个0,
 * 1000！中因子2肯定比因子5多，因此查出因子5的个数即结果0的个数。
 *
 */
int find5(int num){
    int i = 0;
    while(num % 5 == 0){
        num /= 5;
        i++;
    }
    return i;
}

void test1(){
    int num5 = 0;
    for(int i = 5;i < 1001;i++){
       num5 += find5(i); 
    }
    printf("the total zero number is %d\n",num5);
    return ;
}


/**
 * 找到两个字符串中最大的公共子字符串
 * 可输出到屏幕上，也可返回其地址
 */

void getCommon(char *str1,char *str2){
    int len1 = strlen(str1);
    int len2 = strlen(str2);
    int maxlen = 0;
    char *start = NULL;
    char * end  = NULL;

    for(int i = 0;i < len1;i++){
        for(int j = 0;j < len2;j++){
            if(str1[i] == str2[j]){
                int a = i;
                int b = j;
                int count = 1;
                while(a + 1 < len1 && b + 1 < len2 && str1[++a] == str2[++b])
                    count++;
                if(count > maxlen){
                    maxlen  = count;
                    start   = &str1[i];
                    end     = &str1[i + count];
                }
           }
        }
    }

    while(start < end){
        printf("%c",*start);
        start++;
    }
    printf("\nmaxlen:%d",maxlen);

    return ;
}

struct B{
    char t :4;
    char k :4;
    unsigned short i:8;
    unsigned long m;
}b; // sizeof struct B is 16 : 8字节对齐



//得到一个数二进制表示形式中1的个数
int getOneNumber(int num){
    int count = 0;
    int temp  = 0;
    while(num){
        temp = num & 0x01;
        if(temp)
            count++;
        num >>= 1;
    }
    return count;

/***************
    // another way
    while(num){
        count++;
        num = num & (num - 1);
    }
    return count;

***************/
}

struct s{
    int i :8;
    int j :4;
    int a :5;
    int b;
} as = {1,2,16,10};
/* 
 *
 * memcpy
 *
 */
void *my_memcpy(void *dest,const void *src,int length){
    assert(dest != NULL && src != NULL);
    char* ptr = dest;
    const char* s = src;

    if(dest < src + length && dest > src){
        while(length-- > 0){
            *(ptr + length) = *(s + length);
        }
    }

    while(length-- > 0){
       *ptr++ = *s++;
    }
    return dest;
}


/**
 * 设计一个算法，使得数组s[N]中所有的奇数在所有的偶数之前.
 * 要求不增加存储空间，时间复杂度为O(n)
 */
void test2(int s[],int size){
    int *a = s;
    int *b = s + size - 1;

    for(int i = 0;a < b;i++){
        printf("i: %d\n",i);
        if((*a & 0x01 )== 1){
            a++;
        }else if((*b & 0x01 )== 0){
            b--;
        }else{
            *a ^= *b;
            *b ^= *a;
            *a ^= *b;
        }
    }

    /*
    int i,j;
    j = size - 1;
    for(i = 0;i < j;){
        printf("i: %d\n",i);
        if((s[i] & 0x01) == 0){
            s[i] ^= s[j];
            s[j] ^= s[i];
            s[i] ^= s[j];
            j--;
        }else{
            i++;
        }
    }
    
    */
    return ;
}

int main(int argc, const char *argv[])
{
    /*
    printf("sizeof test:%ld\n",sizeof(test));

    printf("a:%d\n",test.a); //1
    printf("b:%d\n",test.b); //-1
    printf("c:%d\n",test.c); //-1
    

    int b = 3;
    foo(b += 2,++b);
    printf("b:%d\n",b);

    test1();
    getCommon("abcacabcabc","abcdefgabcabc");
    putchar(10);

    printf("sizeof unsigned short:%d\n",sizeof (unsigned short));
    printf("sizeof unsigned long:%d\n",sizeof (unsigned long));
    printf("sizeof b : %d\n",sizeof (struct B));

    int count = getOneNumber(9999);
    printf("count:%d\n",count);


    printf("sizeof :%lu\n",sizeof(struct s));
    printf("&i :%x\n",*((char*)&as + 1));
    printf("&j :%x\n",*((char*)&as + 2));
    printf("&b :%p\n",&as.b);
*/
#ifdef MY_MEMCPY 
    char a[10] = "helloworld";
    int b[10] = {0};
/*  my_memcpy(b,a,5);

    for(int i = 0;i < 5;i++){
        printf("%c ",*((char*)b + i));
    }
*/
    my_memcpy(&a[3],a,5); // 位置重叠的时候
    puts(a); 
    my_memcpy(a,&a[4],5);
    puts(a);
    putchar(10);
#endif

#ifdef TEST2 
    int a[10] = {1,2,3,4,5,6,7,8,9,0};
    test2(a,10);
    for(int i = 0;i < 10;i++){
        printf("a : %d\n",a[i]);
    }
#endif
    
   return 0;

}
