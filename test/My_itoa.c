#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*
 * 将input字符串中的最长的数字串输出
 * output 指向这个字符串,函数返回最
 * 长字符串的长度
 */

int countNum(char *output,const char *input);
int my_atoi(const char*str);
char* my_itoa(int value,char* string,int radix);

int main(int argc, const char *argv[])
{
    char * str = "fda123s2323223abc1233dfd23233232323";
    char * result = (char*)malloc(64);
    printf("longest number string :%s len:%d\n",result,countNum(result,str));

    printf("my_itoa:%d\n",my_atoi("1200"));

    char num[16];
    printf("%s\n",my_itoa(120,num,10));


    return 0;
}
int countNum(char *output,const char *input){
    const char  *max    =  NULL;
    int         max_len = 0;     
    const char  *str = input;

    for(;*str != '\0';str++){
        if(*str >= '0' && *str <= '9'){
            const char *ptr = str;
            int i = 0;
            for(;*str >= '0' && *str <= '9';str++,i++)
                ;
            if(i > max_len){
                max = ptr;
                max_len = i;
            }
        }
    }
    memcpy(output,max,max_len * sizeof(char));
    return max_len;
}
/*
 * 将一个字符串转换为整数
 *
 */
int my_atoi(const char*str){
    const char* temp = str;
    int sum     = 0;
    int flag    = 1;


    if(*temp == '-'){
         flag = -1;
         temp++;
    }
    while(*temp){
        if(*temp < '0' || *temp > '9'){
            puts("error input");
            return 0;
        }
        sum = sum * 10 + *temp - '0'; 
        temp++;
    }
    return sum * flag;
}
/*
 * value :要转换的数据
 * string：转换之后的字符串
 * radix：转换后的进制
 */
char* my_itoa(int value,char* string,int radix){
    char *const temp = string;
    char dict[17] = "0123456789abcdef";

    if(radix < 2){
        puts("error radix");
        return NULL;
    }
    if(value < 0)
        value = -value;

    while(value){
        *string++ = dict[value % radix];
        value /= radix;
        if(value < radix){
            *string++ = dict[value];
            *string = '\0';
            break;
        }
    }
    for(int i = 0,j = string - temp - 1;i < (string - temp) / 2;i++,j--){
        temp[i] ^= temp[j];
        temp[j] ^= temp[i];
        temp[i] ^= temp[j];
    }
    return temp;
}
