#include<stdio.h>
#include<stdlib.h>
#include<error.h>
/*
 * 从文件1中读取数字，反向输出到第二个文件中。
 * 两种方式，第一种方式具有扩展内存
 * 第二种方式应用了反向递归调用。
 *
 */
#define DATA_MAX 10

void test1(FILE* ,FILE* ,int*);
void inreverse(FILE *fp_a,FILE* fp_b);

int main(int argc, const char *argv[])
{
    int *p_a = (int*)malloc(sizeof(int) * DATA_MAX);
    
    FILE* fp_a;
    FILE* fp_b;

    if((fp_a = fopen("file1","r")) == NULL){
        perror("open file1 error");
        exit(-1);
    }

    if((fp_b = fopen("file2","w")) == NULL){
        perror("open file2 error");
        exit(-1);
    }

//    test1(fp_a,fp_b,p_a);
    inreverse(fp_a,fp_b);

    fclose(fp_a);
    fclose(fp_b);
    
    return 0;
}
/*
 * fp_a : source file
 * fp_b : destination file
 * p_a  : buffer array
 */
void test1(FILE* fp_a,FILE* fp_b,int* p_a){

    int i = 0;
    int j = 0;
    int reAlloc = DATA_MAX;

    while(fscanf(fp_a,"%d",p_a + i) != EOF){
        i++;
        j++;
        if(i > reAlloc){
            reAlloc += DATA_MAX;
            int *b = (int*)realloc(p_a,reAlloc * sizeof(int));
            printf("reAlloc :%d\n",reAlloc);
            if(b != NULL){
                p_a = b;
            }else{
                perror("recalloc error");
                exit(-1);
            }
        }
    }

    while(j-- > 0){
        fprintf(fp_b,"%d\n",*(p_a + j));
    }
}
/*
 * fp_a : source file
 * fp_b : destination file
*/

void inreverse(FILE *fp_a,FILE* fp_b){
    char buf[1024] = {0};
    if(fgets(buf,sizeof(buf),fp_a) == NULL)
        return;
    inreverse(fp_a,fp_b);
    fputs(buf,fp_b);
            
}
