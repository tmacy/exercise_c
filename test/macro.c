#include<stdio.h>

#define ADD(A,B) (A + B)
#define MUL(A,B) ((A) * (B))
#define MIN(A,B) (A) <= (B) ? (A) : (B)

int main(int argc, const char *argv[])
{
    int a;
    int b;

    puts("input a b ");
    scanf("%d%d",&a,&b);

    printf("the result of a * b is :%d\n",MUL(a,b));
//  if you input like this
    printf("the sum of a+b and a-b is %d\n",MUL(a + b, a - b));

    printf("the minimum of a and b is %d\n",MIN(a,b));
    printf("the minimum of a + b and b is %d\n",MIN(a + b,b));
    int *p = &a;
    printf("a:%d\n,b:%d\n",*p,b);
    printf("the minimum of *p++ and b is %d\n",MIN(*p++,b)); // p指针往后移动了，所以每次输出都是b的值
    printf("the minimum of *p++ and b is %d\n",MIN((*p)++,b));


    return 0;
}
