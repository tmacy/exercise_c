/*************************************************************************
	> File Name: beep.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月04日 星期五 18时09分15秒
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<signal.h>
#include<unistd.h>


void handler(int sig){
    static int time = 0;

    printf("BEEP\n");
    if (++time < 5){
        signal(SIGALRM,handler);
        alarm(1);
    }else{
        printf("BOOM\n");
        exit(0);
    }
}


int main(void){

    signal(SIGALRM,handler);
    alarm(1);

    while(1);

    exit(0);
}
