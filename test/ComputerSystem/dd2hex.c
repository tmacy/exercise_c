/*************************************************************************
	> File Name: dd2hex.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月12日 星期六 11时01分07秒
 ************************************************************************/

#include<stdio.h>
#include<arpa/inet.h>

int main(int argc,char** argv){

    if(argc < 2){
        printf("usage is %s arg \n",argv[0]);
        return 0;
    }

    struct in_addr in;

    printf("%d\n",inet_aton(argv[1],&in));
    printf("%#x\n",ntohl(in.s_addr));

    return 0;
}
