/*************************************************************************
	> File Name: pracitce2.2.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年05月01日 星期四 14时45分14秒
 ************************************************************************/

#include<stdio.h>

int main(int argc ,char** argv){
    int x = 0x87654321;

    printf("change x to 0x00000021 :%0#10x\n",(x & 0xff));
    printf("change x to 0x789abc21 :%#x\n",(x & 0xff | 0x789abc00));
    printf("change x to 0x876543ff :%#x\n",(x | 0xff));

    return 0;
}
