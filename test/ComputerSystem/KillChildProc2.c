/*************************************************************************
	> File Name: KillChildProc2.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月04日 星期五 22时56分02秒
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<signal.h>
#include<wait.h>
#include<errno.h>

void handler(int sig){
    pid_t pid;

    //循环回收子进程
    while((pid = waitpid(-1,NULL,0)) > 0) 
        printf("handler child %d\n",(int)pid);

    if(errno != ECHILD)
        printf("waitpid error\n");

    sleep(2);
    return ;
}


int main(void){


    if(signal(SIGCHLD,handler) == SIG_ERR){
        puts("signal error");
    }

    for (int i = 0;i < 3;i++){
        if(fork() == 0){
            printf("Hello form child :%d\n",(int)getpid());
            sleep(1);
            exit(0);
        }
    }

    int n  = 0;
    char buf[64];
    //发送SIGCHLD信号时，会中断read系统调用，需要手动重启read调
    //用，用循环即可
    while((n = read(STDIN_FILENO,buf,sizeof(buf))) < 0){
        if(errno != EINTR)
            printf("read error\n");
    }

    printf("Parent processing input\n");

    exit(0);
}

