/*************************************************************************
	> File Name: echoclient.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月14日 星期一 13时11分54秒
 ************************************************************************/

#include<stdio.h>
#include"csapp.h"

#define MAX 1024

int main(int argc ,char ** argv){
    int clientfd,port;
    rio_t rio;
    char * host;
    char buf[MAX];
    

    if(argc != 3){
        fprintf(stderr,"Usage :%s <host> <port> \n",argv[0]);
        exit(0);
    }

    host = argv[1];
    port = atoi(argv[2]);

    if( (clientfd = open_clientfd(host,port)) < 0){
        perror("open_clientfd error");
        exit(-1);
    }

    rio_readinitb(&rio,clientfd);

    while(fgets(buf,MAX,stdin) != NULL){
        rio_writen(clientfd,buf,sizeof(buf));
        rio_readlineb(&rio,buf,MAX);
        fprintf(stdout,"from server :%s",buf);
    }

    close(clientfd);

    exit(0);
}
