/*************************************************************************
	> File Name: hex2dd.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月12日 星期六 10时48分21秒
 ************************************************************************/

#include<stdio.h>
#include<arpa/inet.h>

int main(int argc,char** argv){
    
    if(argc < 2){
        printf("usage needs one arg\n");
        return 0;
    } 

    struct in_addr in;
    const char * cp = argv[1];

    inet_aton(cp,&in);

    printf("ip address is %s\n",inet_ntoa(in));
    
    return 0;
}
