/*************************************************************************
	> File Name: copyfile.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月12日 星期六 13时41分51秒
 ************************************************************************/

#include<stdio.h>
#include"csapp.h"

rio_t rio;  // buffer



int main(int argc, char **argv){
    size_t n; 
    char buf[100];
    int inputfd;

    if(argc != 2){
        printf("usage : %s <filename> \n",argv[0]);
        exit(-1);
    }

    if (( inputfd = open(argv[1],O_RDONLY,0) ) < 0){
        perror("open error");
        exit(-1);
    }

    bzero(&rio,sizeof(rio));

    rio_readinitb(&rio,inputfd);

    while( (n = rio_readlineb(&rio,buf,sizeof(buf))) > 0){
        rio_writen(STDOUT_FILENO,buf,n);
    }

/*
    while( (n = rio_readn(inputfd,buf,sizeof(buf))) > 0){
        rio_writen(STDOUT_FILENO,buf,n);
    }
*/
    close(inputfd);
    exit(0);
}
