/*************************************************************************
	> File Name: testOverFlow_add.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年05月06日 星期二 16时53分37秒
 ************************************************************************/

#include<stdio.h>
#include<limits.h>

/* 
 * 无符号数加减法越界判断
 */
 int uadd_ok(unsigned int x,unsigned int y){
     return !(x + y < x);
 }
 int usub_ok(unsigned int x,unsigned y){
//     return !y || !uadd_ok(x,-y);
     return x < y;

 }
 int umul_ok(unsigned int x,unsigned y){
    unsigned int result = x * y;
     return !x && y == result / x;
 }


/*
 *有符号数加减法越界判断
 */
int tadd_ok(int x,int y){
    return !(x < 0 && y < 0 && x + y > 0) || ( x > 0 && y > 0 && x + y < 0 );
}
int tsub_ok(int x,int y){

    return y == INT_MIN ? x > 0 : tadd_ok(x,-y);
}

int tmul_ok(int x,int y){
    return umul_ok(x,y);
}

int main(void){
    int a = INT_MIN;
    int b = -2;
    printf("test for overflow_add of a and b :%d\n",tadd_ok(a,b));
    printf("test for overflow_sub of a and b :%d\n",tsub_ok(a,b));
    return 0;
}
