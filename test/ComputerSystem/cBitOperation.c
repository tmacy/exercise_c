/*************************************************************************
	> File Name: cBitOperation.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年05月01日 星期四 14时57分34秒
 ************************************************************************/

#include<stdio.h>

/* 
 *
 * function bis sets bits of x to 1 where mask m is 1
 * function bic sets bits of x to 0 where mask m is 1 
 *
 */
int bis(int x,int m);
int bic(int x,int m);

/*
 * function bool_or overrides operation or
 *
 * function bool_xor overrides operation xor
 *
 **/
int bool_or(int x,int y);
int bool_xor(int x,int y);

int func1(unsigned int w);
int func2(unsigned int w);


int main(int argc,char** argv){

    int a = 5; //0101
    int b = 3; //0011

    printf("a | b is %#x\n",bool_or(a,b));
    printf("a ^ b is %#x\n",bool_xor(a,b));


    printf("0x00000076 func1 :%#x func2 :%#x\n",func1(0x00000076),func2(0x00000076));
    printf("0x87654321 func1 :%#x func2 :%#x\n",func1(0x87654321),func2(0x87654321));
    printf("0x000000c9 func1 :%#x func2 :%#x\n",func1(0x000000c9),func2(0x000000c9));
    printf("0xedcba987 func1 :%#x func2 :%#x\n",func1(0xedcba987),func2(0xedcba987));
    

    return 0;
}



int bis(int x,int m){
    return x | m;
}
int bic(int x,int m){
    return x & ~m;
}

int bool_or(int x,int y){
    int result = bis(x,y);
    return result;
}
int bool_xor(int x,int y){
    int result = bis(bic(x,y),bic(y,x));
    return result;
}
/**
 * func1 and func2 is practice problem 2.23 on page 74
 */

int func1(unsigned int w){
     return (int) ((w << 24) >> 24);
 }

int func2(unsigned int w){
     return ((int)w << 24) >> 24;
 }
