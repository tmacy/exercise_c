/*************************************************************************
	> File Name: csapp.h
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月12日 星期六 12时56分01秒
 ************************************************************************/

#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<signal.h>
#include<errno.h>
#include<string.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>

#define RIO_BUFSIZE 1024 

typedef struct {
    int rio_fd;  //Descriptor for this internal buf
    int rio_cnt;  //Unread bytes in internal buf
    char *rio_bufptr;// Next unread byte in internal buf
    char rio_buf[RIO_BUFSIZE]; /* Internal buffer */ 
} rio_t;

typedef struct sockaddr SA;


ssize_t rio_readn(int fd,void *usrbuf,size_t n);
ssize_t rio_writen (int fd,void *usrbuf,size_t n);

void rio_readinitb(rio_t *rp ,int fd);
ssize_t rio_readnb(rio_t *rp ,void *usrbuf,size_t n);

static ssize_t rio_read (rio_t *rp ,char *usrbuf,size_t n);
ssize_t rio_readlineb(rio_t *rp ,void *usrbuf,size_t maxlen);
ssize_t rio_readnb(rio_t *rp ,void  *usrbuf,size_t n);


int open_clientfd(char *hostname, int port);
int open_listenfd(int port);
