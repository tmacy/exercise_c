/*************************************************************************
	> File Name: kill.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月04日 星期五 17时45分01秒
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<signal.h>
#include<unistd.h>


pid_t Fork(void){
    pid_t pid;

    if ((pid = fork()) < 0)
        printf("fork error\n");
    return pid;
}

int main(void){
    pid_t pid;
    
    if((pid = Fork()) == 0) {

        printf("I am child process\n");
        pause();
        printf("control should never reach here!\n");
        exit(0);
    }

    printf("Farther will kill child process\n");
    kill(pid,SIGKILL);
    exit(0);
}
