/*************************************************************************
	> File Name: setjmp.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月07日 星期一 12时44分51秒
 ************************************************************************/

#include<stdio.h>
#include<setjmp.h>
#include<stdlib.h>
#include<sys/types.h>
#include<signal.h>
#include<unistd.h>
#include<time.h>

jmp_buf buf;

#define ERROR1 0
#define ERROR2 2

void foo(void);
void bar(void);


int main(void){
    int rc;

    rc = setjmp(buf);

    if(rc == 0){
        foo();
    }else if(rc == ERROR1){
        printf("Detected an error1 condition in foo\n");
    }else if(rc == ERROR2){
        printf("Detected an error2 condition in bar\n");
    }else{
        printf("Unknow error condition in foo\n");
    }


    exit(0);
}


void foo(void){
    if(ERROR1){
        longjmp(buf,1);
    }
    bar();
}
void bar(void){
    if(ERROR2){
        longjmp(buf,2);
    }
}
