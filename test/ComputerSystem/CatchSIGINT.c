/*************************************************************************
	> File Name: signalTest.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月04日 星期五 19时32分09秒
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>


void handler(int sig){
    printf("Caught SIGINT\n");
    exit(0);
}

int main(void){
    signal(SIGINT,handler);
    pause();
    exit(0);
}
