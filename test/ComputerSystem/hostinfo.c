/*************************************************************************
	> File Name: hostinfo.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年07月12日 星期六 11时22分47秒
 ************************************************************************/

#include<stdio.h>
#include<netdb.h>
#include<arpa/inet.h>
#include<stdlib.h>

int main(int argc,char** argv){

    struct hostent *hostp;
    struct in_addr addr;
    char **pp ;

    if(argc != 2){
        fprintf(stderr,"usage:%s <domain name or dotted-decimal> \n",argv[0]);
        exit(0);
    }

    if (inet_aton(argv[1],&addr) != 0){
        hostp = gethostbyaddr(&addr,sizeof(addr),AF_INET);
    }else{
        hostp = gethostbyname(argv[1]);
    }

    printf("official hostname :%s\n",hostp->h_name);

    for(pp = hostp->h_aliases; *pp != NULL; pp++){
        printf("alias :%s\n",*pp);
    }


    for( pp = hostp->h_addr_list;*pp != NULL; pp++){
        addr.s_addr = ((struct  in_addr *)*pp)->s_addr;
        printf("address :%s\n",inet_ntoa(addr));
    }

    return 0;
}
