#include<stdio.h>
#include<math.h>

/*
 * 
 *input s number like 5
 *print the combination of the 5
 *1,4
 *2,3
 *
 *input a number then print it's factor
 *
 */

void combina(int );
void factor(const int );


int main(int argc, const char *argv[])
{
    int num = 0;

    puts("input a number");
    scanf("%d",&num);

    combina(num);
    factor(num);

    return 0;
}

void combina(int a){
    int i ,j;

    printf("the combination of %d is:\n",a);
    for(i = 0,j = a;i != j && i < j;i++,j--){
        printf("%d,%d\n",i,j);
    }
}


void factor(const int a){
    printf("the factor of %d is: \n",a);
    for(int i = 1;i <= sqrt(a);i++){
        if(a % i == 0)
            printf("%d,%d\n",i , a / i);
    }

}
