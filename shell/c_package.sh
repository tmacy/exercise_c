#! /bin/bash
##src是有原始资料的文件夹
##path是打包的目标文件夹
src=$1
path=$2

echo "enter $src"
cd $src

##filelist 保存了当前文件下所有的文件名
filelist=$(ls)

##判断这个目录存在与否，不存在就创建
if [ ! -d $path ]
then
    mkdir "$path"
fi

echo "start"

##遍历所有文件，开始打包

for file in $filelist
do
    if [ -f $file ]
    then

        first=0
        second=0
        third=0
        fourth=0
##创建名称为t的有名管道（进程间通信）
##awk和shell是俩进程，之间传参数需要进程间通信
        if [ ! -p t ]
        then
            mkfifo t
        fi

        echo $file | awk '{print split($0,nameArray,"-");printf("first=%s\n",nameArray[1]);
        printf("second=%s\n",nameArray[2]);printf("third=%s\n",nameArray[3]);printf("fourth=%s\n",nameArray[4]);
        printf("end\n");}' > t &

        while read c
        do
            test "$c" == "end" && break;
            eval $c
        done  < t
         
        rm t

        echo $first
        echo $second
        echo $third
        echo $fourth


        fourth=${fourth:0:4}
        echo $fourth

        if [ ! -d $path/$first ]
        then
            echo "create $path/$first"
            mkdir "$path/$first"
        fi
        cp $file $path/$first

        cd  $path/$first

        if [ ! -d ./$fourth ]
        then
            echo "create ./$fourth"
            mkdir $fourth
        fi
#tar每次打包会把拷贝来的所有xml文件打包，覆盖到下一层目录离去
        tar -cvf $second.tar.gz $first-$second-*-$fourth*.xml
        mv $second.tar.gz ./$fourth 
        cd $src 
    fi
done

#删除所有多余的xml文件
cd $path
filelist=$(ls)
for file in $filelist
do 
     if [ -d $file ]
      then
          cd  $file
          rm *.xml
          cd ..
      fi
done

echo "done"


