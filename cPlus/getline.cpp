/*************************************************************************
	> File Name: getline.cpp
	> Author: 
	> Mail: 
	> Created Time: 2014年10月04日 星期六 10时42分32秒
 ************************************************************************/

#include<iostream>
using namespace std;

int main(void){
    const int ArySize = 16;
    char name[ArySize];
    char dessert[ArySize];
    char ch;

    cout << "enter your name:\n";
    cin.getline(name,ArySize);
    cout << "enter your favorite dessert:\n";
    cin.get(dessert,ArySize);
    cin.get(ch);
    cout << "I have some dessert " << dessert;
    cout << " for you."<< name << endl;
    cout << int(ch)<<endl;
    return 0;
}
