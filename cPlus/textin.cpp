/*************************************************************************
	> File Name: textin.cpp
	> Author: 
	> Mail: 
	> Created Time: 2014年10月08日 星期三 16时35分19秒
 ************************************************************************/

#include<iostream>
#include<stdio.h>
using namespace std;


int main(void){
    int ch;
    int count = 0;

    while((ch = cin.get()) != EOF){  //EOF needs file "stdio.h"
        cout.put(char(ch));
        ++count;
    }
    cout << endl << count << "characters read\n";
    return 0;
}
