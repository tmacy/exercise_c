/*************************************************************************
	> File Name: waiting.cpp
	> Author: 
	> Mail: 
	> Created Time: 2014年10月08日 星期三 15时38分07秒
 ************************************************************************/

#include<iostream>
#include<ctime>

using namespace std;

int main(void){
    cout << "enter the delay time, in seconds : " ;
    int secs;

    cin >> secs;

    cout << "starting!\n";
    cout << secs<< endl;

    while(secs != 0){
    
        clock_t start = clock();
        while((clock() - start) < CLOCKS_PER_SEC)  // 到达一秒时
        ;
        cout << --secs << endl; 
    }
    cout << "done\n";
    return 0;
}

