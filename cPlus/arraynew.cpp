/*************************************************************************
	> File Name: arraynew.cpp
	> Author: 
	> Mail: 
	> Created Time: 2014年10月08日 星期三 11时50分36秒
 ************************************************************************/

#include<iostream>
using namespace std;


int main(void){
    int * array = new int[10];
    int a [10] = {0};

    for(int i = 0;i < 10;i++){
        array[i] = i;
    }

    for(int i = 0;i < 10;i++){
        cout<< array[i] <<endl;
    }
    delete [] array;

    cout << sizeof(array) <<endl;
    cout << sizeof(a) << endl;

    /*
    for(int i = 0;i < 10;i++){
        cout<< array[i] <<endl;
    }*/
    return 0;
}
