/*************************************************************************
	> File Name: comparing.cpp
	> Author: 
	> Mail: 
	> Created Time: 2014年10月08日 星期三 15时23分06秒
 ************************************************************************/

#include<iostream>
#include<cstring>
using namespace std;


int main(void){
    char word[5] = "?ate";

    for(char c = 'a'; strcmp("zate",word) != 0 ;c++){
        cout << word << endl;
        word[0] = c;
    }
    cout << "After loop ends :" << word <<endl;


    /******************Other Version***************/

    string words = "?ate";
    for (char c = 'a';words != "mate";c++){
        cout << words <<endl;
        words[0] = c;
    }

    cout << "Other version word :" << words<< endl;
    cout <<(words == word )<< endl;

    return 0;
}
