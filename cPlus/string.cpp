/*************************************************************************
	> File Name: string.cpp
	> Author: 
	> Mail: 
	> Created Time: 2014年10月04日 星期六 11时07分26秒
 ************************************************************************/

#include<iostream>
#include<string>
using namespace std;

int main(void){
    string str1 = "hello" ;
    string str2;
    const char * ch = "world";
    
    str2 = str1;
    if(str2 == str1){
        cout << "str2 == str1\n";
        cout << &str1 <<endl << &str2<<endl;
    }

    cout<< sizeof(str1)<<endl;
    cout<< sizeof(ch) << endl;

    return 0;
}
