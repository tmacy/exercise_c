/*************************************************************************
	> File Name: con_static.c
	> Author: 
	> Mail: 
	> Created Time: 2015年01月07日 星期三 22时47分45秒
 ************************************************************************/

#include"errors.h"
#include<pthread.h> 

typedef struct my_struct_tag{
    pthread_mutex_t     mutex;
    pthread_cond_t      cond;
    int                 value;
} my_struct_t;


my_struct_t data = { PTHREAD_MUTEX_INITIALIZER,PTHREAD_COND_INITIALIZER,0}; 

int main(int argc,char *argv[]){
    printf("data : %d,%d,%d\n",data.mutex,data.cond,data.value);  
    return 0;
}
