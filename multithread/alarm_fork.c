/*************************************************************************
	> File Name: alarm_fork.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年12月31日 星期三 10时07分13秒
    > note: main函数创建子进程，在子进程中异步调用sleep函数，父进程
    > 等待。
 ************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(void){
    int seconds;
    char line[128];
    char message[64];

    int status;
    pid_t pid;


    while(1){
        printf("Alarm>");
        if(fgets(line,sizeof(line),stdin) == NULL) exit(0);
        if(strlen(line) <= 1) continue;

        if(sscanf(line,"%d %64[^\n]",&seconds,message) < 2 ){
            fprintf(stderr,"Bad command\n");
        }else{
            pid = fork();
            if( pid < 0){
                perror("Fork");
            }
            if(0 == pid){
                sleep(seconds);
                printf("(%d) %s\n",seconds,message);
                exit(0);
            }else{
                do{
                    pid = waitpid(-1,NULL,WNOHANG);
                }while(pid);
            }
        }
    }
    return 0;
}
