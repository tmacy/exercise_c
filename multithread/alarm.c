/*************************************************************************
	> File Name: alarm.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年12月31日 星期三 10时07分13秒
    > note:同步闹钟程序，单循环，通过fgets获取终端输入，第一个参数是睡眠时间，
    > 第二个参数是提示信息.打印信息最多64个字符。
 ************************************************************************/

#include"errors.h"

int main(void){
    int seconds;
    char line[128];
    char message[64];

    while(1){
        printf("Alarm>");
        if(fgets(line,sizeof(line),stdin) == NULL) exit(0);
        if(strlen(line) <= 1) continue;

        if(sscanf(line,"%d %64[^\n]",&seconds,message) < 2 ){
            fprintf(stderr,"Bad command\n");
        }else{
            sleep(seconds);
            printf("(%d) %s\n",seconds,message);
        }
    }
    return 0;
}
