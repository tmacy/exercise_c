/*************************************************************************
	> File Name: lifecycle.c
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年12月31日 星期三 
    > note : 显示一个线程的生命周期,thread_routine函数返回一个符合标准
    > 线程函数原型的值（void*) ，调用pthread_create函数创建线程，调用
    > pthread_join等待线程结束。
 ************************************************************************/

#include<stdio.h>
#include"errors.h"
#include<pthread.h>

void * thread_routine(void* arg){
    printf("a thread is created ! after 5 seconds thread will be done\n");
    sleep(5);
    printf("thread end !\n"); 
    return arg;
}

int main(void){
    pthread_t thread_id;
    void * thread_result = NULL;
    int status;
    int arg = 1;


    status = pthread_create(&thread_id,NULL,thread_routine,&arg);
    if(status != 0){
        err_abort(status,"create thread");
    }
    
    if((status = pthread_join(thread_id,&thread_result)) != 0){
        err_abort(status,"Join thread");
    }
    if(thread_result ==  NULL){
        printf("thread_result is NULL\n");
    }else{
        printf("thread result : %d\n",*(int*)thread_result);
    }
    return 0;
}
