/*************************************************************************
	> File Name: errors.h
	> Author: chen
	> Mail: chen_tmacy@foxmail.com
	> Created Time: 2014年12月31日 星期三 14时29分45秒
 ************************************************************************/
#ifndef __ERRORS_H
#define __ERRORS_H

#include <unistd.h>
#include<errno.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*
 * Define a macro that can be used for diagnostic output from examples.
 * when compiled -DDEBUG, it results in calling printf with the specified
 * argument list. When DEBUG is not defined, it expends to nothing.
 */
#define DEBUG

 #ifdef DEBUG
 #define DPRINTF(arg) printf arg
 #else
 #define DPRINTF(arg)
 #endif

#define err_abort(code,text) do{ \
    fprintf (stderr, "%s at \" %s \" :%d %s\n",\
             text,__FILE__,__LINE__,strerror(code));\
    abort();\
    }while(0)

#define errno_abort(text) do{\
    fprintf(stderr,"%s at\" %s \" :%d :%s\n",\
            text, __FILE__,__LINE__,strerror(errno));\
    abort();\
    }while(0)

#endif



